from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=250, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    author = models.CharField(max_length=250, null=True, blank=True)
